# Train Ticket Machine
Spring Boot Train Ticket Machine

# Technologies
Java 11  
Spring Boot  
Swagger UI  

# How to run
To run the application you only need to start Spring Boot (Server Port: 8080) and access the link below to access the Search train stations endpoint: 
[Swagger UI](http://localhost:8080/swagger-ui.html)
